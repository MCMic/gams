<?php

require __DIR__ . '/vendor/autoload.php';

use MCMic\Gemini;

if (count($argv) < 3) {
    die('Missing args');
}

chdir($argv[1]);
$boardUri = $argv[2];

$stateFilePath = './info.gmi';

$stateFile = fopen($stateFilePath, 'r+');
if ($stateFile === false) {
    throw new Exception('Could not read ' . $stateFilePath);
}

$linksToAdd = [];

while (($line = fgets($stateFile)) !== false) {
    if (preg_match('/^=>/', $line) !== 1) {
        continue;
    }
    $infos      = explode(' ', trim($line));
    $lastUpdate = $infos[4] ?? null;
    echo "Querying " . $infos[1] . "…\n";
    $request = new Gemini\Request($infos[1]);
    $response = $request->sendTo();
    if ($response->status !== 20) {
        echo "Failed to fetch " . $infos[1] . ": " . $response;
        continue;
    }
    $lines = explode("\r\n", ($response->body ?? ''));
    foreach ($lines as $line2) {
        if (preg_match('/^=>/', $line2) !== 1) {
            continue;
        }
        if (strpos($line2, $infos[3]) === false) {
            continue;
        }
        $line2 = preg_replace('/^=>\s?/', '', $line2);
        $line2Data = explode(' ', ($line2 ?? ''), 3);
        if (count($line2Data) !== 3) {
            continue;
        }
        [$link, $date, $title] = $line2Data;
        try {
            $dateObject = new DateTime($date);
            if ($lastUpdate !== null) {
                if ($dateObject < $lastUpdate) {
                    continue;
                }
            }
        } catch (Exception $e) {
            echo $e . "\n";
            continue;
        }
        // Resolve link
        $link = $request->resolveLink($link)->buildUrl();
        // Load page and find first link
        $pageRequest = new Gemini\Request($link);
        $pageResponse = $pageRequest->sendTo();
        if ($pageResponse->status !== 20) {
            echo "Failed to fetch " . $link . ": " . $pageResponse;
            continue;
        }
        $pageLines = explode("\n", ($pageResponse->body ?? ''));
        $parentLink = null;
        foreach ($pageLines as $pageLine) {
            if (preg_match('/^=>\s?([^ ]+)/', $pageLine, $m) !== 1) {
                continue;
            }
            $parentLink = $request->resolveLink($m[1])->buildUrl();
            break;
        }
        if ($parentLink === null) {
            echo "Failed to find parent link for " . $link . "\r\n";
            continue;
        }
        $linksToAdd[$parentLink][$link] = [$date, $infos[2], $title];
    }
    fseek($stateFile, -11, SEEK_CUR);
    fwrite($stateFile, date('Y-m-d'));
}
if (!feof($stateFile)) {
    throw new Exception('Unexpected fgets() fail');
}
fclose($stateFile);

if (count($linksToAdd) === 0) {
    echo "No new threads\n";
    exit();
}

// Add new threads

foreach (($linksToAdd[$boardUri] ?? []) as $link => $infos) {
    $thread = './threads/' . substr(sha1($link), 0, 10) . '.gmi';
    if (file_exists($thread)) {
        continue;
    }
    $threadFile = fopen($thread, 'w');
    if ($threadFile !== false) {
        fwrite($threadFile, "=> $link $infos[0] $infos[1] $infos[2]\r\n");
        fclose($threadFile);
    }
}
unset($linksToAdd[$boardUri]);

if (count($linksToAdd) === 0) {
    echo "No new messages\n";
    exit();
}

// Browse existing threads and add messages

$threads = glob('./threads/*.gmi');

if ($threads !== false) {
    foreach ($threads as $thread) {
        $lines = file($thread);
        if ($lines === false) {
            echo "Could not read $thread\n";
            continue;
        }

        $i = 0;
        for ($i = 0; $i < count($lines); ++$i) {
            if (preg_match('/^=>\s?([^ ]+)(\s-+\s)?(.+)$/', $lines[$i], $m) !== 1) {
                echo "no ".$lines[$i]."\n";
                continue;
            }
            if (isset($linksToAdd[$m[1]])) {
                foreach ($linksToAdd[$m[1]] as $link => $info) {
                    [$date, $author, $title] = $info;
                    $newLine = "=> $link ".trim($m[2])."- $date $author $title\r\n";
                    if (!in_array($newLine, $lines, true)) {
                        // FIXME Inefficient to do in_array here
                        array_splice($lines, $i+1, 0, [$newLine]);
                        echo "Added $link to $thread\r\n";
                    }
                }
            }
            unset($linksToAdd[$m[1]]);
            if (count($linksToAdd) === 0) {
                break;
            }
        }
        $res = file_put_contents($thread, $lines);
        if ($res === false) {
            echo "Could not write $thread\n";
            continue;
        }
        if (count($linksToAdd) === 0) {
            echo "No more messages\n";
            break;
        }
    }
}

// Dump failed links
var_dump($linksToAdd);
